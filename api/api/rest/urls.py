from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^clients/$', views.ClientList.as_view()),
    url(r'^projects/$', views.ProjectList.as_view()),
    url(r'^agile_models/$', views.ManageAgileModels.as_view()),
]