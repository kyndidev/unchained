# from django.shortcuts import render
# from .models import Clients
# from api.rest.serializers import ClientsSerializer
#
# from rest_framework import viewsets
#
# # Create your views here.
#
# class ClientsViewSet(viewsets.ModelViewSet):
#     queryset = Clients.objects.all()
#     serializer_class = ClientsSerializer
from django.db.models import Count, Q
from django.http import HttpResponse
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import Clients, Projects, Kmodels, Labels
from .serializers import ClientsSerializer, ProjectSerializer, ManageAgileModelsSerializer

# class JSONResponse(HttpResponse):
#     """
#     An HttpResponse that renders its content into JSON.
#     """
#     def __init__(self, data, **kwargs):
#         content = JSONRenderer().render(data)
#         kwargs['content_type'] = 'application/json'
#         super(JSONResponse, self).__init__(content, **kwargs)
#
# def client_list(request):
#     print(request)
#     if request.method == 'GET':
#         clients = Clients.objects.all()
#         serializer = ClientsSerializer(clients, many=True)
#         return JSONResponse(serializer.data)

class ClientList(APIView):
    def get(self, request, format=None):
        all_clients = Clients.objects.all()
        serializer = ClientsSerializer(all_clients, many=True)
        return Response(serializer.data)

class ProjectList(APIView):
    def get(self, request, format=None):
        all_projects = Projects.objects.all()
        serializer = ProjectSerializer(all_projects, many=True)
        return Response(serializer.data)

class ManageAgileModels(APIView):
    def get(self, request, format=None):
        agile_models = Kmodels.objects.all()\
            .annotate(pos_labels=(Count('labels__kmodelid', filter=Q(labels__label=True))))\
            .annotate(neg_labels=(Count('labels__kmodelid', filter=Q(labels__label=False))))
        # agile_models = KModel.objects.annotate(true_count=Count())
        serializer = ManageAgileModelsSerializer(agile_models, many=True)
        return Response(serializer.data)


