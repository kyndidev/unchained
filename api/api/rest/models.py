# from django.db import models
#
# # Create your models here.
#
# class Client(models.Model):
#     clientId = models.CharField(db_column='clientid', max_length=100, primary_key=True)
#     name = models.CharField(db_column='name', max_length=100)
#     createdAt = models.DateTimeField(db_column='createdat')
#     updatedAt = models.DateTimeField(db_column='updatedat')
#
#     class Meta:
#         db_table = 'summit.clients'
#
# class Entity(models.Model):
#     cogspaceId = models.UUIDField()
#     kGraphId = models.CharField(max_length=100)
#     entity = models.CharField(max_length=100)
#     kyndiRank = models.FloatField()
#     tagType = models.CharField(max_length=100)
#     tag = models.CharField(max_length=100)
#     confidence = models.FloatField()
#     posTag = models.CharField(max_length=100)
#
#     class Meta:
#         db_table = 'entities'
#
# class EntityKModelScore(models.Model):
#     kModelId = models.CharField(max_length=100)
#     cogspaceId = models.UUIDField()
#     kGraphId = models.CharField(max_length=100)
#     entity = models.CharField(max_length=100)
#
# class KModel(models.Model):
#     kmodel_id = models.CharField(db_column='kmodelid', max_length=100, primary_key=True)
#     client_id = models.CharField(db_column='clientid', max_length=100)
#     name = models.CharField(db_column='name', max_length=100)
#
#     class Meta:
#         db_table = 'kmodels'
#
# class Label(models.Model):
#     cogspace_id = models.CharField(db_column='cogspaceid', max_length=100)
#     provenance_id = models.CharField(db_column='provenanceid', max_length=100, primary_key=True)
#     kmodel_id = models.ForeignKey(KModel, db_column='kmodelid', on_delete=models.CASCADE)
#     client_id = models.CharField(db_column='clientid', max_length=100)
#     label = models.BooleanField()
#     created_at = models.DateTimeField(db_column='createdat')
#     updated_at = models.DateTimeField(db_column='updatedat')
#
#     class Meta:
#         db_table = 'labels'
#
#
#
#
# class Project(models.Model):
#     cogspaceId = models.UUIDField(primary_key=True)
#     clientId = models.UUIDField()
#     #clientId = models.ForeignKey('Client', on_delete=models.CASCADE())
#     name = models.CharField(max_length=100)
#     lifecycle = models.CharField(max_length=100)
#
#     class Meta:
#         db_table = 'projects'

from django.db import models


class Clients(models.Model):
    id = models.IntegerField(primary_key=True)
    field_id = models.UUIDField(db_column='_id', blank=True, null=True)  # Field renamed because it started with '_'.
    clientid = models.TextField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    createdat = models.DateTimeField(blank=True, null=True)
    updatedat = models.DateTimeField(blank=True, null=True)
    field_v = models.IntegerField(db_column='__v', blank=True, null=True)  # Field renamed because it contained more than one '_' in a row. Field renamed because it started with '_'.

    class Meta:
        managed = False
        db_table = 'clients'


class Kmodelfeatures(models.Model):
    id = models.IntegerField(primary_key=True)
    field_id = models.UUIDField(db_column='_id', blank=True, null=True)  # Field renamed because it started with '_'.
    featureid = models.TextField(blank=True, null=True)
    featuretype = models.TextField(blank=True, null=True)
    definition = models.TextField(blank=True, null=True)
    featurename = models.TextField(blank=True, null=True)
    clientid = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'kmodelfeatures'


class Kmodels(models.Model):
    id = models.IntegerField()
    field_id = models.UUIDField(db_column='_id', blank=True, null=True)  # Field renamed because it started with '_'.
    kmodelid = models.TextField(primary_key=True, blank=True)
    kmodeltype = models.TextField(blank=True, null=True)
    isactive = models.BooleanField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    clientid = models.TextField(blank=True, null=True)
    lifecycle = models.TextField(blank=True, null=True)
    lastbuildstart = models.DateTimeField(blank=True, null=True)
    createdat = models.DateTimeField(blank=True, null=True)
    updatedat = models.DateTimeField(blank=True, null=True)
    lastbuildexpired = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'kmodels'


class KmodelsContributingfeatures(models.Model):
    id = models.IntegerField(primary_key=True)
    parent_fk = models.ForeignKey(Kmodels, models.DO_NOTHING, db_column='parent_fk', blank=True, null=True)
    index = models.IntegerField(blank=True, null=True)
    string = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'kmodels_contributingfeatures'


class KmodelsProcessingerrors(models.Model):
    id = models.IntegerField(primary_key=True)
    parent_fk = models.ForeignKey(Kmodels, models.DO_NOTHING, db_column='parent_fk', blank=True, null=True)
    index = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'kmodels_processingerrors'


class Labels(models.Model):
    id = models.IntegerField(primary_key=True)
    field_id = models.UUIDField(db_column='_id', blank=True, null=True)  # Field renamed because it started with '_'.
    cogspaceid = models.TextField(blank=True, null=True)
    provenanceid = models.TextField(blank=True, null=True)
    kmodelid = models.ForeignKey(Kmodels, db_column='kmodelid', on_delete=models.CASCADE)
    clientid = models.TextField(blank=True, null=True)
    label = models.BooleanField(blank=True, null=True)
    createdat = models.DateTimeField(blank=True, null=True)
    updatedat = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'labels'


class Projects(models.Model):
    id = models.IntegerField(primary_key=True)
    field_id = models.UUIDField(db_column='_id', blank=True, null=True)  # Field renamed because it started with '_'.
    clientid = models.TextField(blank=True, null=True)
    projectid = models.TextField(blank=True, null=True)
    lifecycle = models.TextField(blank=True, null=True)
    mountid = models.TextField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    createdat = models.DateTimeField(blank=True, null=True)
    updatedat = models.DateTimeField(blank=True, null=True)
    cogspaceid = models.TextField(blank=True, null=True)
    userid = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    processingstart = models.DateTimeField(blank=True, null=True)
    processingend = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'projects'


class ProjectsProcessingerrors(models.Model):
    id = models.IntegerField(primary_key=True)
    parent_fk = models.ForeignKey(Projects, models.DO_NOTHING, db_column='parent_fk', blank=True, null=True)
    index = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'projects_processingerrors'


class Sentences(models.Model):
    id = models.IntegerField(primary_key=True)
    field_id = models.UUIDField(db_column='_id', blank=True, null=True)  # Field renamed because it started with '_'.
    cogspaceid = models.TextField(blank=True, null=True)
    paperid = models.TextField(blank=True, null=True)
    sentenceid = models.TextField(blank=True, null=True)
    pageindex = models.IntegerField(blank=True, null=True)
    sentenceindex = models.IntegerField(blank=True, null=True)
    sentencetext = models.TextField(blank=True, null=True)
    score = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'sentences'

