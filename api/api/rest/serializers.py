from .models import Clients, Projects, Kmodels
from rest_framework import serializers

class ClientsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Clients
        fields = ('__all__')

class ProjectSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Projects
        fields = ('__all__')

class TrueLabelCountField(serializers.RelatedField):
    def to_representation(self, value):
        return 'trueCount: 1'

class ManageAgileModelsSerializer(serializers.Serializer):
    pos_labels = serializers.IntegerField()
    neg_labels = serializers.IntegerField()
    kmodelid = serializers.CharField()
    name = serializers.CharField()
    createdat = serializers.DateTimeField()
    updatedat = serializers.DateTimeField()

    class Meta:
        model = Kmodels
        fields = '__all__'