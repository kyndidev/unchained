# Generated by Django 2.2.3 on 2019-07-19 18:13

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='clients',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('clientId', models.CharField(max_length=100)),
                ('name', models.CharField(max_length=100)),
                ('createdAt', models.DateTimeField()),
                ('updatedAt', models.DateTimeField()),
            ],
        ),
        migrations.CreateModel(
            name='entities',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cogspaceId', models.UUIDField()),
                ('kGraphId', models.CharField(max_length=100)),
                ('entity', models.CharField(max_length=100)),
                ('kyndiRank', models.FloatField()),
                ('tagType', models.CharField(max_length=100)),
                ('tag', models.CharField(max_length=100)),
                ('confidence', models.FloatField()),
                ('posTag', models.CharField(max_length=100)),
            ],
        ),
    ]
