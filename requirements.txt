dataclasses==0.6
Django==2.2.3
djangorestframework==3.10.1
djongo==1.2.31
psycopg2==2.8.3
pymongo==3.8.0
pytz==2019.1
sqlparse==0.2.4
